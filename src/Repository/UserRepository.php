<?php

namespace App\Repository;

use App\Entity\User;
use App\Service\BackendApi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
	//  v tom user repository bys měl v podstatě naimplementovat to dotahování usera z toho jejich backendu,
	// a nějak to zparsovat do té user entity
	// v user entitě bude zatím to co ti bude vracet ten backend

	private $api;

    public function __construct(RegistryInterface $registry, BackendApi $api)
    {
    	$this->api = $api;
        parent::__construct($registry, User::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.something = :value')->setParameter('value', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

	/**
	 * Get a user entity.
	 *
	 * @param string $param
	 *
	 * @return User
	 */
	public function getUser($param) {
		/** @var User $user */
		return $this->api->getUser($param);
	}

}
